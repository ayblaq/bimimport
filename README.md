
###### HOW TO


To run the code on any python IDE, download [ifcopenshell-python](http://ifcopenshell.org/python.html) and move the extracted archive to your 
python site packages folder.

A first version executable that extracts space information,  and components information from Architectural and Systems IFC models 
have been created. The program converts IFC Artchitectural model to SVG floor plan by making an external call to 
IfcConvert executable. 

This project has been tested with IFC files while running python v3.5.5 on a **Windows 64bit system**.
