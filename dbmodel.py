import mysql.connector as mariadb
from shared import *
import configparser as cp
import logging


class Dbmodel:
    def __init__(self):
        self.connection = None
        self.shared = Shared()

    def openDb(self):
        config = cp.ConfigParser()
        try:
            config.read('dbconfig.txt')
            mariadb_connection = mariadb.connect(user=config['DB']['DB_USERNAME'],
                                                 password=config['DB']['DB_PASSWORD'],
                                                 host=config['DB']['DB_HOST'],
                                                 database=config['DB']['DB_DATABASE'])
            self.connection = mariadb_connection

        except BaseException as e:

            self.shared.writeLog(str(e),'ERROR')
            self.connection = None

    def createTable(self, tablename, tabletype):
        try:
            self.openDb()
            if self.connection is None:
                self.shared.writeLog("Db connection could not be established.", 'ERROR')
                return;
            cur = self.connection.cursor()
            if tabletype == 'space':
                command = """CREATE TABLE IF NOT EXISTS {table} (
                              GUID VARCHAR(36) PRIMARY KEY,
                              ROOMNAME VARCHAR(100) NULL DEFAULT  NULL,
                              FLOOR VARCHAR(25) NULL DEFAULT  NULL,
                              ROOM_TYPE VARCHAR(50) NULL DEFAULT  NULL,
                              CATEGORY VARCHAR(50) NULL DEFAULT  NULL,
                              ROOM_TYPE_ENGLISH VARCHAR(50) NULL DEFAULT  NULL,
                              ROOM_NUMBER VARCHAR(50) NULL DEFAULT  NULL,
                              R8_ROOM_TYPE VARCHAR(50) NULL DEFAULT  NULL,
                              WINDOW INT(11) NULL DEFAULT 0,
                              VOLUME INT(11) NULL DEFAULT 0,
                              AREA FLOAT NULL DEFAULT  NULL,
                              XMIN FLOAT NULL DEFAULT  NULL,
                              YMIN FLOAT NULL DEFAULT  NULL,
                              XMAX FLOAT NULL DEFAULT  NULL,
                              YMAX FLOAT NULL DEFAULT  NULL,
                              LANG VARCHAR(50) NULL DEFAULT NULL,
                              BUIL_ID INT(11) NULL DEFAULT NULL )""".format(table=tablename)

            else:
                command = """CREATE TABLE IF NOT EXISTS {table} (
                              GUID VARCHAR(36) PRIMARY KEY COLLATE 'utf8_bin',
    	                      SYSTEM VARCHAR(256) NULL DEFAULT NULL,
    	                      SYSTEM_ENG VARCHAR(256) NULL DEFAULT NULL,
    	                      SYSTEM_GUID VARCHAR(256) NULL DEFAULT NULL,
    	                      ELEMENT VARCHAR(256) NULL DEFAULT NULL,
    	                      ELEMENTNAME VARCHAR(256) NULL DEFAULT NULL,
    	                      ELEMTYPE VARCHAR(256) NULL DEFAULT NULL,
    	                      DESCRIPTION VARCHAR(128) NULL DEFAULT NULL,
    	                      ROOM_NAME VARCHAR(50) NULL DEFAULT NULL,
    	                      FLOOR VARCHAR(25) NULL DEFAULT NULL,
    	                      XMIN FLOAT NULL DEFAULT NULL,
    	                      YMIN FLOAT NULL DEFAULT NULL,
    	                      XMAX FLOAT NULL DEFAULT NULL,
    	                      YMAX FLOAT NULL DEFAULT NULL,
    	                      BUIL_ID INT(11) NULL DEFAULT NULL,
    	                      P_HEATING_W VARCHAR(50) NULL DEFAULT NULL,
    	                      IntegratedValve VARCHAR(50) NULL DEFAULT NULL,
    	                      kvValue FLOAT NULL DEFAULT NULL,
    	                      ACTUAL_ROOM VARCHAR(50) NULL DEFAULT NULL,
    	                      IMPORT INT(11) NULL DEFAULT NULL,
    	                      IFCTYPE VARCHAR(128) NULL DEFAULT NULL,
    	                      CLOSEST_ROOM VARCHAR(50) NULL DEFAULT NULL,
    	                      2DPLACEMENT VARCHAR(50) NULL DEFAULT NULL,
    	                      qv_SizingFlow_ls VARCHAR(50) NULL DEFAULT NULL,
    	                      dpTot_SizingFlow_Pa VARCHAR(50) NULL DEFAULT NULL,
    	                      CONNECTIONSIZE VARCHAR(50) NULL DEFAULT NULL,
    	                      PredefinedType VARCHAR(50) NULL DEFAULT NULL,
    	                      LANG VARCHAR(50) NULL DEFAULT NULL,
    	                      ObjectId VARCHAR(50) NULL DEFAULT NULL,
    	                      USERCODE VARCHAR(100) NULL DEFAULT NULL,
    	                      CATEGORY VARCHAR(50) NULL DEFAULT NULL,
    	                      FLOWRATE VARCHAR(50) NULL DEFAULT NULL)""".format(table=tablename)
            cur.execute(command)
        except BaseException as e:
            self.shared.writeLog(str(e), 'ERROR')
            print(str(e))

    def insertTable(self,table_name,data):

        if self.connection is None:
            self.shared.writeLog("Db connection could not be established.", 'ERROR')
            return;
        try:
            cur = self.connection.cursor()
            for dt in data:
                placeholder = ", ".join(["%s"] * len(dt))
                update_holder = ', '.join('{}="{}"'.format(k,dt[k]) for k in dt if k != 'GUID')
                stmt = "insert into `{table}` ({columns}) values ({values}) ON DUPLICATE KEY UPDATE {updatestring};".format(
                                                                                    table=table_name,
                                                                                    columns=",".join(dt.keys()),
                                                                                    values=placeholder,
                                                                                    updatestring=update_holder)
                cur.execute(stmt, list(dt.values()))
                self.connection.commit()
            self.shared.writeLog("Populated db successfully.", 'INFO')
        except BaseException as e:
            self.shared.writeLog(str(e),'ERROR')
            print(str(e))