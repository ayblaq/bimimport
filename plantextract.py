from shared import *


class PlantExtract:

    def __init__(self, filepath, building, lang):
        self.filepath = filepath
        self.shared = Shared()
        self.data = Shared.readIfc(filepath)
        self.buildingId = building
        self.lang = lang.lower()

    def singleAttribute(self,obj):

        det = {"SYSTEM_GUID": '', "SYSTEM": None, "GUID": '', "ELEMENT": '', "DESCRIPTION": None,
               "PredefinedType": '', "USERCODE": '', "FLOOR": '',
               "CLOSEST_ROOM": '', "FLOWRATE": '', 'CONNECTIONSIZE': '', 'IFCTYPE': ''}

        ignoredesc = ['Extract air device','Supply air device','Exhaust air device','Outdoor air device']
        try:
            det['IFCTYPE'] = obj.is_a()
            det['GUID'] = obj.GlobalId
            det['GUID'] = obj.GlobalId
            det['DESCRIPTION'] = obj.Description
            if (det['DESCRIPTION'] in ignoreType) or (obj.is_a() not in keyElem):
                msg = "No description exist for this component {obj}".format(obj=obj)
                self.shared.writeLog(msg, 'ERROR')
                return None
            systems = obj.HasAssignments
            if det['DESCRIPTION'] in ignoredesc: return None
            for system in systems:
                det['SYSTEM'] = eval("'%s'" % system.RelatingGroup.Name)
                det['SYSTEM_GUID'] = system.RelatingGroup.GlobalId
            if det['SYSTEM'] is None:
                msg = "No system linked to this component {obj}".format(obj=obj)
                self.shared.writeLog(msg, 'ERROR')
                return None
            if obj.is_a() in identicalElem['flow']:
                vals = obj.IsDefinedBy
                for val in vals:
                    if val.is_a('IfcRelDefinesByProperties'):
                        props = val.RelatingPropertyDefinition
                        if props is not None:
                            if props.is_a('IfcPropertySet'):
                                propis = props.HasProperties
                                type_elem = ""
                                for propi in propis:
                                    propi.Name = eval("'%s'" % propi.Name)
                                    key, value = self.shared.defineType(propi)
                                    if key is None: continue
                                    if key == "ELEMENT":
                                        type_elem = value
                                    det[key] = value
                    if val.is_a('IfcRelDefinesByType'):
                        type_elem = val.RelatingType
                        det['IFCTYPE'] = type_elem.is_a()
                        if type_elem.is_a() in types:  det['DESCRIPTION'] = det['DESCRIPTION'] = types[type_elem.is_a()]
                        det['ELEMENTNAME'] = type_elem.Name
                        if type_elem.is_a('IfcFlowMeterType'):
                            det["USERCODE"] = type_elem[2]

                return det
            if obj.is_a() in identicalElem['device']:
                vals = obj.IsDefinedBy
                type_elem = ""  # type: str
                for val in vals:
                    if val.is_a('IfcRelDefinesByProperties'):
                        props = val.RelatingPropertyDefinition
                        if props is not None:
                            if props.is_a('IfcPropertySet'):
                                propis = props.HasProperties
                                for propi in propis:
                                    propi.Name = eval("'%s'" % propi.Name)
                                    key,value = self.shared.defineType(propi)
                                    if key == "ELEMENT":
                                        type_elem = value
                                    det[key] = value
                    if val.is_a('IfcRelDefinesByType'):
                        det['IFCTYPE'] = val.RelatingType.is_a()
                        if val.RelatingType.is_a() in types:  det['DESCRIPTION'] = types[val.RelatingType.is_a()]
                        type_elem = val.RelatingType.PredefinedType
                        det['ELEMENTNAME'] = type_elem
                        if type_elem is None:
                            type_elem = val.RelatingType.ElementType
                            det['ELEMENTNAME'] = type_elem
                return det
        except:
            msg = "Object type is not an ifc {obj}".format(obj=obj)
            self.shared.writeLog(msg, 'ERROR')
            return None

    def elementsAttribute(self, spacedir):

        elements = []
        prods = self.data
        prods = prods.by_type('IfcBuildingStorey')
        spaces = []
        if spacedir is not None:
            result = self.shared.readIfc(spacedir+'.ifc')
            spaces = result.by_type('IfcSpace')

        for prod in prods:
            elems = prod.ContainsElements
            floor = self.shared.getFloorNum(prod.Name)
            prod.Name = prod.Name.lstrip()
            prod.Name = prod.Name.rstrip()
            msg = "Currently extracting for {comp}".format(comp=prod.Name)
            for elem in elems:
                print(elem)
                vals = elem.RelatedElements
                bar = self.shared.initBar(len(vals))
                index = 0
                for val in vals:
                    if val.is_a() in keyElem:
                        attribute = self.singleAttribute(val)
                        if attribute is None:
                            continue
                        attribute['FLOOR'] = floor
                        attribute['LANG'] = self.lang
                        attribute['BUIL_ID'] = self.buildingId
                        elemcoord = self.shared.getCoord(val)
                        newelemcoord = self.shared.getboundingBox(val)
                        if newelemcoord is not None:
                            attribute['XMIN'], attribute['YMIN'], attribute['ZMIN'],attribute['XMAX'], attribute['YMAX'],attribute['ZMAX'] = newelemcoord
                        for space in spaces:
                            space_floor = self.shared.getFloorNum(space.Name)
                            spName = space.Name
                            pattern = '[0-9]'
                            spName = re.sub(pattern, '', spName)
                            spName = spName.lstrip()
                            if space_floor == floor:
                                rooms = space.IsDecomposedBy

                        closest = self.shared.findClosestSpace(elemcoord, spaces, newelemcoord)
                        attribute['CLOSEST_ROOM'] = closest['closestspace']
                        attribute['ACTUAL_ROOM'] = closest['closestspace']

                        print(attribute)
                        elements.append(attribute)

                    index=index+1
                    bar.update(index)
                bar.finish()
        msg = "Successfully generated Elements for {file}".format(file=self.filepath)
        print(msg)
        self.shared.writeLog(msg, 'INFO')
        return elements

