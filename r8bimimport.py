import argparse
import sys
import os
from dbmodel import *

def parse_arguments():

    parser = argparse.ArgumentParser(description="This is a script for r8bimimport")
    parser.add_argument('-p', dest='plantdir', help="To extract plant components. Takes plant IFC file directory name as input.",
                        default=None, type=dir_path, required=('-l' in sys.argv or '-id' in sys.argv or '-o' in sys.argv) and '-s' not in sys.argv)
    parser.add_argument('-s', dest='spacedir', help="To extract room info and components. Takes space IFC file directory name as input.",
                        default=None, type=dir_path, required=('-l' in sys.argv or '-id' in sys.argv or '-o' in sys.argv) and '-p' not in sys.argv)
    parser.add_argument('--version', action='version', version='%(prog)s 1.0', help="Shows the program's version and exits.")
    if '-p' in sys.argv or '-s' in sys.argv or '-h' in sys.argv or '--help' in sys.argv:
        parser.add_argument('-l', dest='lang',
                            help="The country's-language the building belongs to. For example EN for English, ET for Estonia. Default is set to EN.",
                            default='en')
        parser.add_argument('-id', dest='id', help="Specifies the building Id. Default is set to 0", default=0, type=int)
        parser.add_argument('-tn', dest='tablename',
                            help="Specifies tablename to which the result should be exported to. Required only when export to db is specified.",
                            required=('--export-to-db' in sys.argv))
        parser.add_argument('--export-to-db', dest='outputdb',
                            help="Exports result to a database. Please add db configuration to dbconfig.txt file.",default=False,action='store_true')
        parser.add_argument('--extract-plan', dest='floorplan',default=False,action='store_true',
                            help="Calls IfcConvert.exe to extract floor plans from Ifc files from directory specified in -s. Each Ifc file should represent a floor in the building.")
    return parser.parse_args()


def dir_path(string):
    if os.path.isdir(string) and string is not None:
        return string
    else:
        msg = "Error: Directory {dir} does not exist.".format(dir=string)
        Shared.writeLog(msg)
        sys.exit(msg)

def main(argv):

    plantdir = argv.plantdir
    spacedir = argv.spacedir
    elementType = "plant"
    buildingId = argv.id
    lang = argv.lang
    ext_floor = argv.floorplan
    exporttodb = argv.outputdb
    if spacedir is not None and plantdir is not None:
        elementType = "plant"
    if spacedir is not None and plantdir is None:
        elementType = "space"
    spacefiles = os.listdir(spacedir)
    spacefilename = None

    for filename in spacefiles:
        filename, ext = os.path.splitext(filename)
        if ext == '.ifc':
            spacefilename = "{space}/{file}".format(space=spacedir,file=filename)
            break

    shared = Shared(spacefilename)
    if elementType == "plant":
        files = shared.ifcFilesToList(plantdir)
        for filename in files:
                msg = "Started working on {file}".format(file=filename)
                print(msg)
                shared.writeLog(msg, 'info')
                plant = PlantExtract(filename+'.ifc',buildingId,lang)
                elements = plant.elementsAttribute(spacefilename)
                cols = plant.shared.colFromList(elements)
                plant.shared.listToCsv(elements,filename+'.csv', cols)
                if exporttodb:
                    dbmodel = Dbmodel()
                    dbmodel.createTable(argv.tablename, elementType)
                    dbmodel.insertTable(argv.tablename, elements)
    else:
        if ext_floor:
            files = shared.ifcFilesToList(spacedir)
            for filename in files:
                spaceextract = SpaceExtract(filename)
                spaceextract.extractFloorPlan(filename,spacedir)
        else:
            if spacefilename is not None:
                dbmodel = Dbmodel()
                dbmodel.createTable('testing', elementType)
                msg = "Started extracting room information in {file}".format(file=spacefilename)
                print(msg)
                spaceextract = SpaceExtract(spacefilename+'.ifc',buildingId,lang)
                spaceextract.shared.writeLog(msg, 'info')
                elements = spaceextract.getRoomInfo()
                cols = spaceextract.shared.colFromList(elements)
                spaceextract.shared.listToCsv(elements,spacefilename+'.csv',cols)
                if exporttodb:
                    dbmodel = Dbmodel()
                    dbmodel.createTable(argv.tablename, elementType)
                    dbmodel.insertTable(argv.tablename, elements)
                msg = "Started extracting room component information in {file}".format(file=spacefilename)
                print(msg)
                spaceextract.shared.writeLog(msg, 'info')
                elements = spaceextract.getSpaceComponentInfo()
                cols = spaceextract.shared.colFromList(elements)
                spaceplant = "{file}plant.csv".format(file=spacefilename)
                spaceextract.shared.listToCsv(elements,spaceplant,cols)


if __name__ == '__main__':

    if len(sys.argv) <= 1:
        sys.argv.append('-h')
    parsed_args = parse_arguments()
    print("Launching r8bimimport: v1.0")
    try:
        if parsed_args:
            from plantextract import *
            from spaceextract import *
            main(parsed_args)
    except BaseException as e:
        print(str(e))
        sys.exit(0)