import tkinter as tk
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import askopenfilenames
from concurrent.futures import ThreadPoolExecutor
from tkinter import ttk
from tkinter import filedialog
from dbmodel import *
from plantextract import *
from spaceextract import *
import os
import tkinter.scrolledtext as tkst
import logging
import queue
import threading
import ctypes
from os.path import splitext
import ifcopenshell as ifc
import multiprocessing
import dill as pickle

logger = logging.getLogger(__name__)

executor = ThreadPoolExecutor(8)


class QueueHandler(logging.Handler):

    def __init__(self, log_queue):
        super().__init__()
        self.log_queue = log_queue

    def emit(self, record):
        self.log_queue.put(record)


class App():
    def __init__(self, window):
        self.window = window
        self.window.title('R8 bimimport')
        self.window.grid_columnconfigure(0, weight=1)
        self.window.resizable(width=False, height=False)
        self.plant_frame = tk.Frame(self.window)
        self.space_frame = tk.Frame(self.window)
        self.opt_frame = tk.Frame(self.window)
        self.box_frame = tk.Frame(self.window)
        self.dbbox_frame = tk.Frame(self.window)
        self.close_frame = tk.Frame(self.window)
        self.start_frame = tk.Frame(self.window)
        self.export_frame = ttk.Frame(self.window)
        self.pt = tk.StringVar()
        self.pt.set('space')
        self.pldir_variable = tk.StringVar()
        self.spdir_variable = tk.StringVar()
        self.box_variable = tk.StringVar()
        self.export_variable = tk.IntVar()
        self.building_id = tk.IntVar()
        self.table_name = tk.StringVar()
        self.style = ttk.Style()
        self.style.theme_use('vista')
        self.style.configure('W.TButton', font=
        ('calibri', 10, 'normal'))
        self.style.configure('W.TEntry', font=
        ('calibri', 10, 'normal'))
        self.createRadioButton(self.opt_frame, 'plant', 'plant', 'Extract Plant Info', 1)
        self.createRadioButton(self.opt_frame, 'space', 'space', 'Extract Space Info', 2)
        self.createRadioButton(self.opt_frame, 'svg', 'space', 'IFC to SVG', 3)
        self.opt_frame.grid(row=0, column=0, columnspan=5, pady=(0, 10))
        self.plant_frame.grid(row=2, column=0, columnspan=5)
        self.space_frame.grid(row=3, column=0, columnspan=5)
        self.export_frame.grid(row=4, column=0, columnspan=5)
        self.dbbox_frame.grid(row=5, column=0, columnspan=5)
        self.box_frame.grid(row=6, column=0, columnspan=5)
        self.start_frame.grid(row=7, column=0, columnspan=5)
        self.close_frame.grid(row=7, column=0, columnspan=5)
        ttk.Label(self.export_frame, text='Export to db').grid(row=0, column=0)
        tk.Checkbutton(self.export_frame, variable=self.export_variable, command=self.hideDBInput).grid(row=0, column=1)
        ttk.Label(self.dbbox_frame, text='Building Id (Default 0)').grid(row=0, column=0)
        ttk.Entry(self.dbbox_frame, textvariable=self.building_id, style="W.TEntry", width=5).grid(row=0, column=1)
        ttk.Label(self.dbbox_frame, text='Table name').grid(row=1, column=0, pady=(4, 0))
        ttk.Entry(self.dbbox_frame, textvariable=self.table_name, style="W.TEntry").grid(row=1, column=1, pady=(4, 0))
        self.editArea = tkst.ScrolledText(self.box_frame, wrap=tk.WORD, width=50, height=10, font=("Calibri", 10))
        self.editArea.grid(pady=(20, 10))
        self.editArea.configure(state='disabled')
        ttk.Button(self.start_frame, text="Start", command=self.launchApp).grid(column=0, row=1)
        ttk.Button(self.close_frame, text="Stop", command=self.closeApp).grid(column=0, row=1)
        self.createTextField('Plant IFC', 'plant', self.pldir_variable, 3)
        self.createTextField('Space IFC', 'space', self.spdir_variable, 4)
        self.plant_frame.grid_remove()
        self.close_frame.grid_remove()
        self.dbbox_frame.grid_remove()
        m = multiprocessing.Manager()
        self.log_queue = m.Queue()
        self.queue_handler = QueueHandler(self.log_queue)
        formatter = logging.Formatter('%(asctime)s: %(message)s')
        self.queue_handler.setFormatter(formatter)
        logger.addHandler(self.queue_handler)
        self.ifcQueue = multiprocessing.Queue()
        self.window.after(10, self.poll_log_queue)
        self.current_process = None
        # self.window.after(10, self.poll_ifc_queue)

    def createRadioButton(self, frame, value, ttype, text, col):
        ttk.Radiobutton(self.opt_frame, text=text, value=value, variable=self.pt,
                        style='Options_Radio.TRadiobutton', command=lambda: self.resetFrame1(ttype)).grid(column=col,
                                                                                                          row=0)

    def hideDBInput(self):
        val = self.export_variable.get()
        if val == 0:
            self.dbbox_frame.grid_remove()
            self.building_id.set(0)
            self.table_name.set('')
        else:
            self.dbbox_frame.grid()

    def display(self, record):

        if isinstance(record, str):
            self.editArea.configure(state='normal')
            self.editArea.insert(tk.END, record + '\n', record)
        else:
            msg = self.queue_handler.format(record)
            self.editArea.configure(state='normal')
            self.editArea.insert(tk.END, msg + '\n', record.levelname)
        self.editArea.configure(state='disabled')
        # self.editArea.yview(tk.END)

    def resetFrame1(self, type='plant'):
        self.plant_frame.grid_remove()
        self.pldir_variable.set('')
        if type == 'plant':
            self.plant_frame.grid()

    def createTextField(self, text, frame, box, row):
        view = self.space_frame
        if frame == 'plant':
            view = self.plant_frame
        tk.Label(view, text=text, fg="black").grid(row=row, column=0)
        ttk.Entry(view, textvariable=box, style="W.TEntry").grid(row=row, column=1)
        ttk.Button(view, text='Browse...', command=lambda: self.getFilename(box, frame), style="W.TButton").grid(
            row=row,
            column=2)

    def raise_exception(self):
        thread_id = self.t.ident
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id,
                                                         ctypes.py_object(SystemExit))
        if res > 1:
            ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, 0)
            print('Exception raise failure')

    def closeApp(self):

        self.close_frame.grid_remove()
        self.start_frame.grid()
        if self.current_process:
            self.current_process.terminate()
            self.current_process.join()

    def file_path(self, filepath):
        if os.path.isfile(filepath) and filepath is not None:
            filepath, ext = splitext(filepath)
            if ext == '.ifc':
                return filepath
            else:
                return None
        else:
            return None

    def poll_log_queue(self):
        while True:
            try:
                record = self.log_queue.get(block=False)
                if isinstance(record, dict):
                    self.closeApp()
                else:
                    self.display(record)

            except queue.Empty:
                break
            else:
                # self.display(record)
                print(record)
        self.window.after(10, self.poll_log_queue)

    @staticmethod
    def processSVG(spacedir, spacefilename, log_queue=None):
        msg = "Generating svg from {file} \n \n".format(file=spacedir)
        spaceextract = SpaceExtract(spacefilename)
        spaceextract.shared.log_queue = log_queue
        spaceextract.shared.writeLog(msg, 'INFO')
        spaceextract.extractFloorPlan(spacefilename)
        spaceextract.shared.writeLog('DONE........', 'INFO')
        log_queue.put({'closeapp': True})

    @staticmethod
    def processSpaceExtract(obj, log_queue):
        spacefilename = obj['spacefilename']
        buildingId = obj['buildingId']
        lang = obj['lang']
        exporttodb = obj['exporttodb']
        tablename = obj['tablename']
        msg = "Started extracting room information\n".format()
        spaceextract = SpaceExtract(spacefilename + '.ifc', buildingId, lang)
        spaceextract.shared.log_queue = log_queue
        spaceextract.shared.writeLog(msg, 'INFO')
        elements = spaceextract.getRoomInfo()
        cols = spaceextract.shared.colFromList(elements)
        spaceextract.shared.listToCsv(elements, spacefilename + '.csv', cols)
        msg = "Started extracting room component information \n".format()
        spaceextract.shared.writeLog(msg, 'INFO')
        elements = spaceextract.getSpaceComponentInfo()
        cols = spaceextract.shared.colFromList(elements)
        spaceplant = "{file}plant.csv".format(file=spacefilename)
        spaceextract.shared.listToCsv(elements, spaceplant, cols)
        msg = "Room information exported to {filename}.csv".format(filename=spacefilename)
        spaceextract.shared.writeLog(msg, 'INFO')
        if exporttodb:
            spaceextract.shared.writeLog('Export to database not yet supported.', 'INFO')
            if tablename is not None:
                spaceextract.shared.writeLog('Exporting output to db.', 'INFO')
                dbmodel = Dbmodel()
                dbmodel.shared.log_queue = log_queue
                dbmodel.createTable(tablename, "space")
                dbmodel.insertTable(tablename, elements)
            else:
                spaceextract.shared.writeLog('Table name cannot be Empty.', 'ERROR')
        msg = "Started extracting room component information in {file}".format(file=spacefilename)
        spaceextract.shared.writeLog(msg, 'INFO')
        elements = spaceextract.getSpaceComponentInfo()
        cols = spaceextract.shared.colFromList(elements)
        spaceplant = "{file}plant.csv".format(file=spacefilename)
        spaceextract.shared.listToCsv(elements, spaceplant, cols)
        msg = "Room components exported to {filename}plant.csv".format(filename=spacefilename)
        spaceextract.shared.writeLog(msg, 'INFO')
        spaceextract.shared.writeLog('DONE.......', 'INFO')
        log_queue.put({'closeapp': True})

    @staticmethod
    def processPlantExtract(obj, log_queue):

        plantfilename = obj['plantfilename']
        buildingId = obj['buildingId']
        lang = obj['lang']
        spacefilename = obj['spacefilename']
        tablename = obj['tablename']
        exporttodb = obj['exporttodb']
        msg = "Extracting comp information in {file}".format(file=plantfilename)
        plant = PlantExtract(plantfilename + '.ifc', buildingId, lang)
        plant.shared.log_queue = log_queue
        plant.shared.writeLog(msg, 'INFO')
        elements = plant.elementsAttribute(spacefilename)
        cols = plant.shared.colFromList(elements)
        plant.shared.listToCsv(elements, plantfilename + '.csv', cols)
        if exporttodb:
            if tablename is not None:
                plant.shared.writeLog('Exporting output to db.', 'INFO')
                dbmodel = Dbmodel()
                dbmodel.shared.log_queue = log_queue
                dbmodel.createTable(tablename, "plant")
                dbmodel.insertTable(tablename, elements)
            else:
                plant.shared.writeLog('Table name cannot be Empty.', 'INFO')
        log_queue.put({'closeapp': True})

    def launchApp(self):
        option = self.pt.get()
        spdir = self.spdir_variable.get()
        pldir = self.pldir_variable.get()
        try:
            buildingId = int(self.building_id.get())
        except:
            logger.log(logging.ERROR, 'Please enter an Integer.')
        lang = 'en'
        exporttodb = self.export_variable.get()
        self.start_frame.grid_remove()
        self.close_frame.grid()
        tablename = self.table_name.get()
        shared = Shared()
        shared.logger = logger
        spacedir = self.file_path(spdir)
        plantdir = self.file_path(pldir)
        spacefilename = spacedir
        msg = "Running app for {info} \n".format(info=option)
        shared.writeLog(msg, 'INFO')
        if option == 'svg' or option == 'space':
            if spacedir is None:
                msg = "Error: File {dir} does not exist. \n".format(dir=spdir)
                logger.log(logging.ERROR, msg)
                self.closeApp()
                return
            if option == 'svg':

                p = multiprocessing.Process(target=self.processSVG, args=(spacedir, spacefilename, self.log_queue))
                p.start()
                self.current_process = p
            else:
                if spacefilename is not None:
                    obj = {'tablename': tablename, 'buildingId': buildingId, 'lang': lang,
                           'spacefilename': spacefilename, 'exporttodb': exporttodb}
                    p = multiprocessing.Process(target=self.processSpaceExtract, args=(obj, self.log_queue))
                    p.start()
                    self.current_process = p
        elif option == 'plant':
            if plantdir is None:
                msg = "Error: File {dir} does not exist. \n".format(dir=plantdir)
                logger.log(logging.ERROR, msg)
                self.closeApp()
                return
            obj = {'tablename': tablename, 'buildingId': buildingId, 'lang': lang, 'spacefilename': spacefilename,
                   'exporttodb': exporttodb, 'plantfilename': plantdir}
            print(obj)
            p = multiprocessing.Process(target=self.processPlantExtract, args=(obj, self.log_queue))
            p.start()
            self.current_process = p

    def getFilename(self, box, frame):

        options = {'parent': self.window, 'initialdir': os.getcwd(), 'title': 'R8 File Import', 'filetypes': '*ifc'}
        file = filedialog.askopenfilenames(filetypes=[("IFC files", "*.ifc")], initialdir=os.getcwd(),
                                           parent=self.window)
        shared = Shared()
        shared.logger = logger
        try:
            if len(file) > 0:
                if frame == 'space':
                    self.spdir_variable.set(file[0])
                else:
                    self.pldir_variable.set(file[0])
                    logger.log(logging.INFO, "{file} added successfully".format(file=file[0]))
        except BaseException as e:
            print(str(e))
            msg = "{error}\n".format(error=str(e))
            logger.log(logging.ERROR, msg)


if __name__ == "__main__":
    app = App(tk.Tk())
    app.window.mainloop()
