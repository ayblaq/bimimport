import ifcopenshell as ifc
import ifcopenshell.geom as IfcGeom
from scipy.spatial import distance
from ifcopenshell import geom
import re
import pandas as pd
import logging
import sys
import os
import translators as ts
import OCC
import OCC.BRepBndLib
import progressbar
import numpy as np
from io import StringIO

identicalElem = {'flow': ['IfcFlowMovingDevice', 'IfcFlowController', 'IfcFlowStorageDevice', 'IfcFlowTreatmentDevice',
                          'IfcFlowTerminal', 'IfcBuildingElementProxy'],
                 'device': ['IfcEnergyConversionDevice']}
keyElem = ['IfcFlowMovingDevice', 'IfcFlowController', 'IfcFlowTerminal', 'IfcFlowStorageDevice',
           'IfcFlowTreatmentDevice', 'IfcEnergyConversionDevice', 'IfcBuildingElementProxy']
types = {'IfcTankType': 'Tank', 'IfcValveType': 'Valve'}
ignoreType = ['Silencer', 'Fire damper', 'Flow damper', 'Drainage device', 'Domestic water device',
              'Drainage component']

p_prop = {'terminal': 'IfcFlowTerminal',
          'controller': 'IfcFlowController',
          'flowdevice': 'IfcFlowMovingDevice',
          'distports': 'IfcDistributionPort',
          'product': 'IfcProduct',
          'system': 'IfcSystem',
          'floor': 'IfcBuildingStorey'
          }

global_langs = {'en': "https://translate.google.en",
                'et': "https://translate.google.ee",
                'fi': "https://translate.google.fi",
                'lv': "https://translate.google.lv",
                'lt': "https://translate.google.lt"}

progressbar.streams.wrap_stderr()
progressbar.streams.flush()
log_stream = StringIO
# filename='bimimport.log'
logging.basicConfig(level=logging.INFO, filename='bimimport.log', format='%(asctime)s :: %(levelname)s :: %(message)s')


class Shared:
    def __init__(self):
        self.settings = geom.settings()
        self.settings.set(self.settings.USE_PYTHON_OPENCASCADE, True)
        self.connection = None
        self.logger = None
        self.log_queue = None

    def getboundingBox(self, obj):
        try:
            prod = IfcGeom.create_shape(self.settings, obj).geometry
            bbox = OCC.Bnd.Bnd_Box()
            tol = 1e-6
            bbox.SetGap(tol)
            OCC.BRepBndLib.brepbndlib_Add(prod, bbox, False)
            return bbox.Get()
        except BaseException as e:
            print(str(e))

    @staticmethod
    def readIfc(filepath):
        data = None
        try:
            data = ifc.open(filepath)
        except:
            msg = "File {file} is not an IFC file".format(file=filepath)
            Shared.writeLog(msg, "ERROR")
        return data

    @staticmethod
    def rect_distance(elemcoord, spacecoord):

        x1, y1, z1, x1b, y1b, z1b = spacecoord
        x2, y2, z2, x2b, y2b, z2b = elemcoord

        # height1 = abs(abs(y1b) - abs(y1))
        # width1 = abs(abs(x1b) - abs(x1))
        #
        # height2 = abs(abs(y2b) - abs(y2))
        # width2 = abs(abs(x2b) - abs(x2))

        # rect1 = Rect(x1, y1, width1, height1)
        # rect2 = Rect(x2, y2, width2, height2)
        p1 = np.array([x1, y1, z1])
        p2 = np.array([x2, y2, z2])
        squared_dist = np.sum((p1 - p2) ** 2, axis=0)
        return squared_dist;

    def getCoord(self, obj):
        try:
            print(obj)
            prod = IfcGeom.create_shape(self.settings, obj)
            shape = OCC.TopoDS.TopoDS_Iterator(prod.geometry).Value()
            trsf = shape.Location().Transformation()
            x, y, z = trsf.TranslationPart().X(), trsf.TranslationPart().Y(), trsf.TranslationPart().Z()
            return x, y, z

        except BaseException as e:
            msg = "Could not get coordinates for {error}".format(error=str(e))
            self.shared.writeLog(msg, 'ERROR')
            return 0, 0, 0

    def findClosestSpace(self, elemcoord, ress, comp, lst=None):

        if lst is None:
            lst = {'space': '', 'distance': 1000, 'point': '', 'closestspace': '', 'actualspace': ''}
        if elemcoord is not None:
            for res in ress:
                if res.is_a('IfcSpace'):
                    newspacecoord = self.getboundingBox(res)
                    if newspacecoord is not None:
                        val = self.rect_distance(comp, newspacecoord)
                        val = int(val)
                        if val < int(lst['distance']) and res.Name is not None:
                            lst['secd'] = lst['distance']
                            lst['spoint'] = lst['point']
                            lst['distance'] = val
                            lst['sec'] = lst['closestspace']
                            lst['closestspace'] = res.Name
                            lst['point'] = newspacecoord
                            lst['actualspace'] = ''
                        if int(lst['distance']) <= 3 and res.Name is not None:
                            lst['actualspace'] = res.Name
                            lst['closestspace'] = res.Name
                            break
        return lst

    def listToCsv(self, object_list, filename, cols):
        df = pd.DataFrame(object_list, columns=cols)
        df.to_csv(filename, index=False, encoding='utf-8-sig')
        msg = "{filename} file created successfully ".format(filename=filename)
        self.writeLog(msg, 'INFO')
        print(msg)

    def mergeCsv(self, files, filename):
        struct = []
        for file in files:
            a = pd.read_csv(file)
            struct.append(a)
        df = pd.concat(struct)
        df.to_csv(filename, index=False, encoding='utf-8-sig')
        msg = "{filename} file created successfully ".format(filename=filename)
        self.writeLog(msg, 'INFO')

    @staticmethod
    def getFloorNum(name):
        exist = '-' in name
        floor_num = re.search(r'-?\d+', name)
        if floor_num is not None:
            floor = floor_num.group()
        else:
            floor = name
        if exist: floor = '-' + floor
        return floor

    def writeLog(self, string, logType):
        if logType == "WARNING":
            logging.warning(string)
            if self.logger: self.logger.log(logging.WARN, string)
        elif logType == "ERROR":
            logging.error(string)
            if self.logger: self.logger.log(logging.ERROR, string)
        else:
            logging.info(string)
            if self.logger: self.logger.log(logging.INFO, string)
        if self.log_queue:
            # print(logging.warning(string))
            print(logType)
            qLog = "{log}: {string} \n".format(log=logType, string=string)
            self.log_queue.put(qLog)

    def checkDir(self, filepath):
        if not os.path.isdir(filepath):
            msg = "{dir} does not exist ".format(dir=filepath)
            self.writeLog(msg, 'ERROR')
            sys.exit(0)

    def getTranslation(self, word, lang):
        try:
            if lang != 'en':
                word = ts.google(text=word, from_language=lang, to_language='en', host=global_langs[lang],
                                 proxy=None)
            return word
        except BaseException as e:
            msg = "Could not translate {word} from {lang} to English. Main error {error}".format(word=word, lang=lang,
                                                                                                 error=str(e))
            self.writeLog(msg, "WARNING")
            return word

    @staticmethod
    def cleanString(stri):
        stri = eval("'%s'" % stri)
        clean_string = re.sub(r'[?|$|.|!|:|0-9|=|,]', r'', stri)
        clean_string = clean_string.rstrip()
        clean_string = clean_string.lstrip()
        return clean_string

    @staticmethod
    def defineType(propi):
        key, value = None, None
        if propi.Name == "PartType" or propi.Name == "Tüüp":
            key = 'ELEMENT'
            value = propi.NominalValue[0]
        # elif propi.Name == "ProductCode":
        #     key = 'PRODUCTCODE'
        #     value = eval("'%s'" % propi.NominalValue[0])
        elif propi.Name == "User Code":
            key = 'USERCODE'
            value = propi.NominalValue[0]
        elif propi.Name == "ObjectId":
            key = 'ObjectId'
            value = propi.NominalValue[0]
        elif propi.Name == "Predefined Type" or propi.Name == "Toote tüüp":
            key = 'PredefinedType'
            value = eval("'%s'" % propi.NominalValue[0])
        elif propi.Name == "Flow Rate":
            key = 'FLOWRATE'
            value = propi[2][0]
        elif propi.Name == "Description":
            key = 'DESCRIPTION'
            value = propi.NominalValue[0]
        elif propi.Name == "ConnectionSize_mm":
            key = 'CONNECTIONSIZE'
            value = propi[2][0]
        elif propi.Name == "P_Heating_W":
            key = 'P_HEATING_W'
            value = propi[2][0]
        elif propi.Name == "H_Cooling_W":
            key = 'H_COOLING_W'
            value = propi[2][0]
        elif propi.Name == "IntegratedValve":
            key = 'IntegratedValve'
            value = propi.NominalValue[0]
        elif propi.Name == "kvValue":
            key = 'kvValue'
            value = propi.NominalValue[0]
        elif propi.Name == "qv_SizingFlow_ls":
            key = 'qv_SizingFlow_ls'
            value = propi.NominalValue[0]
        elif propi.Name == "qv_SizingFlow_ms":
            key = 'qv_SizingFlow_ms'
            value = propi.NominalValue[0]
        elif propi.Name == "dpTot_SizingFlow_Pa":
            key = 'dpTot_SizingFlow_Pa'
            value = propi.NominalValue[0]
        elif propi.Name == "Storey":
            key = 'FLOOR'
            value = Shared.getFloorNum(propi.NominalValue[0])
        elif propi.Name == "Room name":
            key = 'ROOM_NAME'
            value = propi.NominalValue[0]
        elif propi.Name == "Category":
            key = 'CATEGORY'
            value = propi.NominalValue[0]

        return key, value

    @staticmethod
    def initBar(maxval):
        bar = progressbar.ProgressBar(maxval=maxval,
                                      widgets=[progressbar.Bar('#', '[', ']'), ' ', progressbar.Percentage()],
                                      redirect_stderr=True, stderr_redirection=True, stdout_redirection=True
                                      )
        return bar.start()

    def ifcFilesToList(self, directory):
        files = []
        all_files = os.listdir(directory)
        for filename in all_files:
            filename, ext = os.path.splitext(filename)
            if ext == '.ifc':
                file = "{dir}/{file}".format(dir=directory, file=filename)
                files.append(file)
        if len(files) == 0:
            msg = "No IFC file found in {path}".format(path=directory)
            print(msg)
            self.writeLog(msg, 'ERROR')
        return files

    def colFromList(self, res):
        cols = []
        try:
            cols = list(res[0].keys())
        except BaseException as e:
            self.writeLog(str(e), 'ERROR')
        return cols
