from shared import *
import subprocess
from xml.dom import minidom
import xpath
import io


# import OCC.Bnd as OCCBND


class SpaceExtract:

    def __init__(self, filepath, building=0, lang='en'):
        self.filepath = filepath
        self.data = Shared.readIfc(filepath)
        self.shared = Shared()
        self.buildingId = building
        self.lang = lang.lower()
        self.logger = None

    def getSpaceArea(self, space):

        try:
            prod = IfcGeom.create_shape(self.shared.settings, space).geometry
            bbox = OCC.Bnd.Bnd_Box()
            tol = 1e-6
            bbox.setGap(tol)
            OCC.BRepBndLib.brepbndlib_Add(prod, bbox, False)
            xmin, ymin, zmin, xmax, ymax, zmax = bbox.Get()
            x = abs(xmax) - abs(xmin)
            y = abs(ymax) - abs(ymin)
            z = zmax - zmin
            area = abs(x) * abs(y) * 10.76
            return round(area, 2), xmin, ymin, xmax, ymax

        except BaseException as e:
            msg = "Area could not be computed for {space}. {error}".format(space=space, error=str(e))
            self.shared.writeLog(msg, 'ERROR')
            return 0, 0, 0, 0, 0

    def getRoomInfo(self, ignorespace=[]):

        spaces = self.data.by_type('IfcSpace')
        roomList = []
        trans = {}
        emptyList = {}
        window_declared = False
        if len(spaces) == 0:
            msg = "No spaces/rooms found in file {file}".format(file=self.filepath)
            self.shared.writeLog(msg, 'WARNING')
            return None
        bar = Shared.initBar(len(spaces))
        index = 0
        for prod in spaces:
            # elems = prod.Decomposes
            try:
                prod.LongName = eval("'%s'" % prod.LongName)
                prod.LongName = re.sub('\d+', " ", prod.LongName)
                prod.LongName = re.sub("\.$", " ", prod.LongName)
                prod.LongName = re.sub("\,$", " ", prod.LongName)
                prod.LongName = prod.LongName.split('/')[0]
                if len(prod.LongName.split('-')) > 1:
                    prod.LongName = prod.LongName.split('-')[1]
                prod.Name = eval("'%s'" % prod.Name)
            except:
                prod.LongName = prod.LongName
                prod.Name = prod.Name
                msg = "Error occurred filtering space name {name}".format(name=prod.Name)
                self.shared.writeLog(msg, "WARNING")

            if prod.LongName in ignorespace:
                continue

            if prod.LongName not in trans:
                trans[prod.LongName] = self.shared.getTranslation(prod.LongName, self.lang)
            floor = prod.ObjectPlacement[0].PlacesObject[0].Name
            floor = self.shared.getFloorNum(floor)
            space_props = {'ROOM_TYPE_ENGLISH': trans[prod.LongName], 'ROOM_TYPE': prod.LongName,
                           'ROOM_NUMBER': prod.Name, 'FLOOR': floor, 'WINDOW': '', 'AREA': 0,
                           'BUIL_ID': self.buildingId, 'VOLUME': 0, 'LANG': self.lang.upper(),
                           'GUID': prod.GlobalId, 'CATEGORY': '', 'XMIN': '', 'XMAX': '', 'YMIN': '', 'YMAX': ''}

            if space_props['ROOM_TYPE'] == '':
                emptyList[prod.LongName] = ''
                space_props['ROOM_TYPE'] = prod.Name
            vals = prod.IsDefinedBy
            conE = prod.BoundedBy
            window = 0
            for con in conE:
                typeElem = con.RelatedBuildingElement
                if typeElem is not None:
                    if typeElem.is_a('IfcWindow'):
                        window_declared = True
                        window = window + 1
            space_props['WINDOW'] = window
            space_props['AREA'], space_props['XMIN'], space_props['YMIN'], space_props['XMAX'], space_props[
                'YMAX'] = self.getSpaceArea(prod)
            for val in vals:
                if val.is_a('IfcRelDefinesByProperties'):
                    if val.RelatingPropertyDefinition.is_a('IfcPropertySet'):
                        propis = val.RelatingPropertyDefinition.HasProperties
                        for propi in propis:
                            if propi.Name == "Category":
                                roomtype = propi.NominalValue[0]
                                if roomtype not in trans:
                                    trans[roomtype] = self.shared.getTranslation(roomtype, self.lang)
                                space_props['CATEGORY'] = trans[roomtype]

                            if val.RelatingPropertyDefinition.is_a('IfcElementQuantity'):
                                quantities = val.RelatingPropertyDefinition.Quantities
                                for qty in quantities:
                                    if qty.is_a('IfcQuantityArea'):
                                        space_props['AREA'] = round(qty.AreaValue, 2)
                                    if qty.is_a('IfcQuantitiyVolume'):
                                        space_props['VOLUME'] = round(qty.VolumeValue, 2)
                                    if qty.is_a('IfcPropertySet'):
                                        continue
            # if not window_declared:
            #     space_props['WINDOW'] = self.getSpaceWindow(prod)
            roomList.append(space_props)
            index = index + 1
            bar.update(index)
        # bar.finish
        msg = "Done extracting room information from {file}".format(file=self.filepath)
        self.shared.writeLog(msg, 'INFO')
        return roomList

    def getSpaceWindow(self, space):
        floor = space.ObjectPlacement[0].PlacesObject[0]
        components = floor.ContainsElements
        count = 0
        for component in components:
            windows = component.RelatedElements
            for window in windows:
                if window.is_a('IfcWindow'):
                    newelemcoord = self.shared.getboundingBox(window)
                    elemcoord = self.shared.getCoord(window)
                    closest = self.shared.findClosestSpace(elemcoord, [space], newelemcoord)
                    if closest['distance'] < 1:
                        count = count + 1
        return count

    def getSpaceComponentInfo(self):
        products = self.data.by_type('IfcProduct')
        inTerest = ['IfcBuildingElementProxy', 'IfcDistributionElement', 'IfcDistributionControlElement'
                                                                         'IfcFlowTreatmentDevice',
                    'IfcEnergyConversionDevice', 'IfcFlowController', 'IfcFlowTerminal']
        trans = {}
        elements = []
        for prod in products:
            elem = {'COMPONENT': '', 'COMPONENT_ENG': '', 'CATEGORY': '', 'ROOM_NAME': '', 'FLOOR': '', 'GUID': ''}
            if prod.is_a in inTerest:
                prod.Name = prod.Name.replace(':', " ")
                clean_string = self.shared.cleanString(prod.Name)
                elem['GUID'] = prod.GlobalId
                elem['LANG'] = self.lang
                if len(clean_string) > 3:
                    prod.Name = clean_string
                if prod.Name not in trans:
                    trans[prod.Name] = self.shared.getTranslation(prod.Name, self.lang)
                elem['COMPONENT_ENG'] = trans[prod.Name]
                elem['COMPONENT'] = prod.Name
                structure = prod.ContainedInStructure
                for struct in structure:
                    container = struct.RelatingStructure
                    if container.is_a('IfcSpace'):
                        elem['ROOM_NAME'] = container.Name
                        elem['FLOOR'] = self.shared.getFloorNum(container.ObjectPlacement[0].PlacesObject[0].Name)
                    elif container.is_a('IfcBuildingStorey'):
                        elem['FLOOR'] = container.Name
                vals = prod.IsDefinedBy
                for val in vals:
                    if val.is_a('IfcRelDefinesByProperties'):
                        props = val.RelatingPropertyDefinition
                        if props is not None:
                            if props is not None:
                                propis = props.HasProperties
                                for propi in propis:
                                    key, value = self.shared.defineType(propi)
                                    elem[key] = value

                elements.append(elem)
        return elements

    def splitFloors(self, data):

        floors = data.by_type('IfcBuildingStorey')
        floornames = {}
        for floor in floors:
            floor_num = self.shared.getFloorNum(floor.Name)
            elements = floor.IsDecomposedBy
            floornames[floor_num] = list()
            for elem in elements:
                rooms = elem.RelatedObjects
                for room in rooms:
                    if room.is_a('IfcSpace'):
                        if room.Name is None: continue
                        floornames[floor_num].append(room.Name)

        return floornames

    def extractFloorPlan(self, filename):

        ifc_file = "{filename}.ifc".format(filename=filename)
        filepath = os.path.dirname(ifc_file)
        svg_file = "{filename}.svg".format(filename=filename)

        try:
            os.remove(svg_file)
        except BaseException as e:
            print(str(e))

        msg = "Reading IFC file....."
        self.shared.writeLog(msg, 'INFO')
        data = self.shared.readIfc(ifc_file)
        floornames = self.splitFloors(data)

        try:
            msg = "Converting IFC Geometry"
            self.shared.writeLog(msg, 'INFO')
            cmd = 'IfcConvert "{ifc}" "{svg}" --include entities IfcSpace --bound 1000x1000 --no-normals ' \
                  '--use-world-coords --use-element-names'.format(
                ifc=ifc_file, svg=svg_file)
            subprocess.run(cmd, shell=True)

        except BaseException as e:
            self.shared.writeLog(str(e), 'ERROR')

        msg = "Splitting SVG groups"
        self.shared.writeLog(msg, 'INFO')

        parents = {}
        try:
            doc = minidom.parse(svg_file)
            svgtag = xpath.find('//svg', doc)
        except:
            msg = "Could not generate svg file for {filename}".format(filename=svg_file)
            self.shared.writeLog(msg, 'ERROR')
            return

        for g in svgtag[0].childNodes:
            if g.nodeType != g.TEXT_NODE:
                parents[g.getAttribute('id')] = g
        for rooms in floornames:
            relParents = {}
            for room in floornames[rooms]:
                space = xpath.find('//g[@id = $name]', doc, name='product-' + room)
                for sp in space:
                    sp.setAttribute('id', sp.getAttribute('id').replace('product', 'space'))
                    pid = sp.parentNode.getAttribute('id')
                    parents[pid] = sp.parentNode
                    relParents[pid] = sp.parentNode

            svg_filename = "{path}/{file}.svg".format(path=filepath, file=rooms)
            self.svgToFile(doc, svg_filename, relParents)

        self.svgToFile(doc, svg_file, parents)
        return

    def svgToFile(self, doc, filename, parents):

        svg_tag = doc.createElement('svg')
        svg_tag.setAttribute('xmlns', "http://www.w3.org/2000/svg")
        svg_tag.setAttribute('xmlns:xlink', "http://www.w3.org/1999/svg")
        svg_tag.setAttribute('transform', "scale(1,-1)")
        svg_tag.setAttribute('viewBox', "0 0 1000 476")
        for p in parents:
            parent = parents[p]
            svg_tag.appendChild(parent)
        try:
            with io.open(filename, "w", encoding="utf-8") as f:
                f.write(svg_tag.toprettyxml())
            msg = "Successfully generated {file}".format(file=filename)
            self.shared.writeLog(msg, 'INFO')
        except BaseException as e:
            msg = "{error} while generating {filename}".format(error=str(e), filename=filename)
            self.shared.writeLog(msg, 'ERROR')
